//
//  ViewController.swift
//  ArudinoProjectGate
//
//  Created by Jeremy on 25/05/21.
//

import UIKit
import Foundation

class ViewController: UIViewController {
    @IBOutlet weak var logoImageView: UIImageView!
    var status = 0

    
    let defaultSession = Foundation.URLSession(configuration: URLSessionConfiguration.default)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.systemGreen]
        logoImageView.image = UIImage(named: "gate")!
    }
}

