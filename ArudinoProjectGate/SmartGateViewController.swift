//
//  SmartGateViewController.swift
//  ArudinoProjectGate
//
//  Created by Jeremy on 25/05/21.
//

import UIKit

class SmartGateViewController: UIViewController {

    @IBOutlet weak var presentationImageView: UIImageView!
    @IBOutlet weak var openButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func buttonopen(_ sender: Any) {
        upload(status: "0")
        presentationImageView.image = UIImage(named: "gateOpening")
        disableButton()
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            self.enableButton()
            self.presentationImageView.image = UIImage(named: "gateIdle")
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.upload(status: "3")
        }
        
    }
    
    @IBAction func buttonclose(_ sender: Any) {
        upload(status: "1")
        presentationImageView.image = UIImage(named: "gateClosing")
        disableButton()
        DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
            self.enableButton()
            self.presentationImageView.image = UIImage(named: "gateIdle")
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.upload(status: "3")
        }
    }
    
    @IBAction func buttonstop(_ sender: Any) {
        upload(status: "2")
        presentationImageView.image = UIImage(named: "gateIdle")
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.upload(status: "3")
        }
    }
    
    func disableButton() {
        openButton.isEnabled = false
        closeButton.isEnabled = false
        openButton.setImage(UIImage(named: "progress")!, for: .normal)
        closeButton.setImage(UIImage(named: "progress")!, for: .normal)
    }
    
    func enableButton() {
        openButton.isEnabled = true
        closeButton.isEnabled = true
        openButton.setImage(.none, for: .normal)
        closeButton.setImage(.none, for: .normal)
    }
    
    func upload(status:String) {
        let url = NSURL(string: "http://128.199.242.164/iot/status_update.php")
        var request = URLRequest(url: url! as URL)
        request.httpMethod = "POST"
                
        let dataString = "&status=\(status)"
        let dataD = dataString.data(using: .utf8)!
                
        let uploadJob = URLSession.shared.uploadTask(with: request, from: dataD)
        uploadJob.resume()
    }
    
    func read() {
            let url: URL = URL(string: "http://128.199.242.164/iot/status_read.php")!
            let defaultSession = Foundation.URLSession(configuration: URLSessionConfiguration.default)
            let task = defaultSession.dataTask(with: url) { (data, response, error) in
                    var jsonResult = NSArray()
                    do {
                        jsonResult = try JSONSerialization.jsonObject(with: data!, options:JSONSerialization.ReadingOptions.allowFragments) as! NSArray
                    } catch let error as NSError {
                        print(error)
                    }
                    var jsonElement = NSDictionary()
    
                    for i in 0 ..< jsonResult.count {
                        jsonElement = jsonResult[i] as! NSDictionary
                        if  let status = jsonElement["status"] as? String {
 //                           self.status = Int(status) ?? 0
                        }
                    }
                }
        task.resume()
    }

}
